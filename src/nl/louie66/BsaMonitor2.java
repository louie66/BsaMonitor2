package nl.louie66;

import java.util.Scanner;

public class BsaMonitor2 {
    // Programma om te kijken of je bsa goed of slecht gaat uitpakken
    // @author Rene de Leeuw

    public static void main(String[] args) {

        // De namen array van vakken/ projecten in constanten opslaan
        final String VAKNAMEN[] = {
                "Project Fasten Your Seatbelts",
                "Programming",
                "Databases",
                "Personal Skills",
                "Project Skills",
                "Infrastructure",
                "Network Engineering 1"
        };

        // Deze array bevat de punten te behalen per vak
        // als de cijfers boven een 5.5 zijn
        final int VAKPUNTEN[] = {
                12,
                3,
                3,
                2,
                2,
                3,
                3,
        };

        // Standard input object
        Scanner input = new Scanner(System.in);

        // De array om de cijfers op te slaan de lengte van de array
        // is gelijk aan die van de 2 andere arrays
        double[] cijfers = new double[VAKNAMEN.length];

        int maxLengte = 0; // Houdt bij wat het langste vak is qua karakters (formatting)
        int maxPunten = 0; // Houdt bij wat het maximale aantal haalbare punten is
        int puntenTotaal = 0; // Houdt bij welke punten er in totaal te behalen zijn
        int punten = 0; // Houdt bij welke er door de student behaald zijn

        // Loop om de invoer van de student op te slaan en tellingen bij te houden
        System.out.println("Voer behaalde cijfers in: ");
        for (int i = 0; i < VAKNAMEN.length; i++) {
            System.out.printf("%s: ", VAKNAMEN[i]);
            cijfers[i] = input.nextDouble();
            maxLengte = maxLengte < VAKNAMEN[i].length() ? VAKNAMEN[i].length() : maxLengte;
            maxPunten += VAKPUNTEN[i];
        }

        // Loop om de regels op de console te tonen en tellingen bij te houden
        System.out.println();
        for (int i = 0; i < cijfers.length; i++) {
            punten = (cijfers[i] >= 5.5 ? VAKPUNTEN[i] : 0);
            puntenTotaal += punten;
            System.out.printf("Vak/project: %s Cijfer: %s Behaalde punten: %s\n",
                    rightpad(VAKNAMEN[i], maxLengte), cijfers[i], punten);
        }

        // De student het goede of slechte nieuws tonen
        System.out.println();
        System.out.printf("Totaal behaalde studiepunten: %s/%s\n", puntenTotaal,
                maxPunten);

        if (puntenTotaal < ((maxPunten * 5) / 6)) {
            System.out.print("PAS OP: je ligt op schema voor een negatief BSA!");
        }
    }

    // Static method om spaties uit te vullen
    private static String rightpad(String text, int length) {
        // return String.format("%-8.8s", "Vak1");  print "Vak1" + 4 spaties (8 tekens)
        return String.format("%-" + length + "." + length + "s", text);
    }
}
